require 'sinatra'
require 'stringio'
 
module Kernel
 
  def capture_stdout
    out = StringIO.new
    $stdout = out
    yield
    return out
  ensure
    $stdout = STDOUT
  end
 
end

get '/' do
  'I can run your ruby, gimme!'
end

post '/run' do 
	request.body.rewind
	input = request.body.read	
  input.untaint
  o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
  name = (0...50).map { o[rand(o.length)] }.join
  t=Tempfile.new(name)
  t.write input
  t.close
  read, write = IO.pipe
  pid = fork do
    read.close
    result = capture_stdout do 
      eval(File.open(t.path).read)
    end
#    Marshal.dump(result, write)
    write.write result.string
    write.close
    exit!(0) # skips exit handlers.
  end

  write.close
  result = read.read
  read.close
  Process.wait(pid)

  raise "child failed" if result.empty?
  puts result
  t.delete
  result
end